@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                @if(session('response'))
                    <div class="alert alert-success">{{ session('response') }}</div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <dir class="row">
                            <div class="col-9">Post View</div>
                            <div class="col-3">
                                <form method="POST" action="{{ url('/search') }}">
                                    {{ csrf_field() }}
                                    <div class="input-group">
                                        <input class="form-control mr-sm-2" type="text" name="search" placeholder="Search..." aria-label="Search">
                                        <span class="input-group-btn">
                                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">
                                            Go!
                                        </button>
                                    </span>
                                    </div>
                                </form>
                            </div>
                        </dir>
                    </div>
                        <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                            <div class="row">

                        <div class="col-3">
                            <div class="card-header">
                                Categories
                            </div>
                            <ul class="list-group list-group-flush">
                                    @if(count($categories) > 0 )
                                        @foreach($categories->all() as $category)
                                           <li class="list-group-item">
                                               <a href="{{ url("category/{$category->id}") }}">{{ $category->category }}</a>
                                           </li>
                                        @endforeach
                                    @else
                                        <p>No Category Found!</p>
                                    @endif
                            </ul>
                        </div>

                        <div class="col-9">
                            @if(count($posts) > 0)
                                @foreach($posts->all() as $post)
                                    <h4>{{ $post->post_title }}</h4>
                                    <p>{!! $post->post_body !!}</p>

                                    <ul class="nav nav-pills">
                                        <li role="presentation" class="presentation">
                                            <a href="{{ url("/like/{$post->id}") }}">
                                                <span class="fa fa-thumbs-up"> Like ({{ $likeCount }}) </span>
                                            </a>
                                        </li>
                                        <li role="presentation" class="presentation">
                                            <a href="{{ url("/dislike/{$post->id}") }}">
                                                <span class="fa fa-thumbs-down"> Dislike ({{ $dislikeCount }}) </span>
                                            </a>
                                        </li>
                                        {{--<li role="presentation">--}}
                                            {{--<a href="{{ url("/comment/{$post->id}") }}">--}}
                                                {{--<span class="fa fa-comment"> Comment</span>--}}
                                            {{--</a>--}}
                                        {{--</li>--}}
                                    </ul>
                                @endforeach
                            @else
                                <p>No Posts!</p>
                            @endif

                            <form method="POST" action='{{ url("/comment/{$post->id }") }}'>
                                  {{ csrf_field() }}
                                    <div class="form-group">
                                        <textarea name="comment" id="comment" required></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success btn-lg btn-block">
                                           Add Comment
                                        </button>
                                    </div>
                            </form>

                            <h3 class="comments_title">Comments:</h3>
                                @if(count($comments) > 0)
                                    @foreach($comments->all() as $comment)
                                        <p>{{ $comment->comment }}</p>
                                        <p><span class="comments_posted">Posted by: </span> <span class="comments_posted_user">{{ $comment->name }}</span></p>
                                        <hr>
                                    @endforeach
                                @else
                                    <p>No comments available!</p>
                                @endif
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
