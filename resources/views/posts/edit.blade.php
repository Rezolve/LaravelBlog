@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">Edit Post</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="row justify-content-center">
                            <form class="col-12" method="POST" action="{{ url('/edit', array($posts->id)) }}" enctype="multipart/form-data">
                                @csrf

                                <div class="form-group row">
                                    <label for="post_title" class="col-12">{{ __('Title') }}</label>

                                    <div class="col-12">
                                        <input id="post_title" type="text" class="form-control{{ $errors->has('post_title') ? ' is-invalid' : '' }}" name="post_title" value="{{ $posts->post_title }}" required autofocus>

                                        @if ($errors->has('post_title'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('post_title') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="post_body" class="col-12">{{ __('Post Text') }}</label>

                                    <div class="col-12">
                                        <textarea id="post_body" rows="7" class="form-control{{ $errors->has('post_body') ? ' is-invalid' : '' }}" name="post_body" required>{{ $posts->post_body }}</textarea>

                                        @if ($errors->has('post_body'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('post_body') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="category_id" class="col-2">{{ __('Category') }}</label>

                                    <div class="col-4">
                                        <select id="category_id" type="text" class="form-control{{ $errors->has('category_id') ? ' is-invalid' : '' }}" name="category_id" required>
                                            <option value="{{ $category->id }}">{{ $category->category }}</option>
                                            @if(count($categories) > 0)
                                                @foreach($categories->all() as $category)
                                                    <option value="{{ $category->id }}">{{ $category->category }}</option>
                                                @endforeach
                                            @endif
                                        </select>

                                        @if ($errors->has('category_id'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('category_id') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-4 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Update Post') }}
                                        </button>
                                        <a href="{{ url('/home') }}" class="btn btn-danger">
                                            Back to Homepage
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
