@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <dir class="row">
                            <div class="col-9">Posts View</div>
                            <div class="col-3">
                                <form method="POST" action="{{ url('/search') }}">
                                    {{ csrf_field() }}
                                    <div class="input-group">
                                        <input class="form-control mr-sm-2" type="text" name="search" placeholder="Search..." aria-label="Search">
                                        <span class="input-group-btn">
                                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">
                                            Go!
                                        </button>
                                    </span>
                                    </div>
                                </form>
                            </div>
                        </dir>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-3 show_user_profile">
                                @if(!empty($profile))
                                    <img src="{{ $profile->profile_img }}" class="avatar" alt="">
                                @else
                                    <img src="{{ url('images/no_avatar.png') }}" class="avatar" alt="no avatar img">
                                @endif
                                @if(!empty($profile))
                                    <p class="lead">{{ $profile->name }}</p>
                                @else
                                    <p></p>
                                @endif
                                @if(!empty($profile))
                                    <p class="lead">{{ $profile->designation }}</p>
                                @else
                                    <p></p>
                                @endif
                                <hr>
                                <div class="col-12">
                                    <div class="card-header">
                                        Categories
                                    </div>
                                    <ul class="list-group list-group-flush">
                                        @if(count($categories) > 0 )
                                            @foreach($categories->all() as $category)
                                                <li class="list-group-item">
                                                    <a href="{{ url("category/{$category->id}") }}">{{ $category->category }}</a>
                                                </li>
                                            @endforeach
                                        @else
                                            <p>No Category Found!</p>
                                        @endif
                                    </ul>
                                </div>
                            </div>

                            <div class="col-md-9">
                                @if(count($posts) > 0)
                                    @foreach($posts->all() as $post)
                                        <h4>{{ $post->post_title }}</h4>
                                        <p>{!! substr($post->post_body, 0, 300) !!}</p>

                                        <ul class="nav nav-pills">
                                            <li role="presentation" class="presentation_posts">
                                                <a href="{{ url("/view/{$post->id}") }}">
                                                    <span class="fa fa-eye"> View </span>
                                                </a>
                                            </li>
                                            <li role="presentation" class="presentation_posts">
                                                <a href="{{ url("/edit/{$post->id}") }}">
                                                    <span class="fa fa-pencil-alt"> Edit </span>
                                                </a>
                                            </li>
                                            <li role="presentation" class="presentation_posts">
                                                <a href="{{ url("/delete/{$post->id}") }}">
                                                    <span class="fa fa-trash-alt"> Delete</span>
                                                </a>
                                            </li>
                                        </ul>
                                        <br>
                                        <cite style="float: left;">Posted on: {{ date('M j, Y H:i', strtotime($post->updated_at)) }}</cite>
                                        <br>
                                        <hr />
                                    @endforeach
                                @else
                                    <p style="text-align: center; margin-right: 30%;"><b>No Posts!</b></p>
                                @endif

                                    {{-- pagination start --}}
                                    <div class="row justify-content-center">
                                        <div class="">
                                            {{--{{ $posts->links() }}--}}
                                            {!! $posts->links('pagination') !!}
                                        </div>
                                    </div>
                                    {{-- pagination end --}}

                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
@endsection
