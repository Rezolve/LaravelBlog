<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth-form on home page
Route::get('/', function () {
    return view('auth.login');
});

// NewKernelRoute
Auth::routes();

//  Route::get('/', "HomeController@index")->name('home');

// For auth home page
Route::get('/home', 'HomeController@index')->name('home');

// PostController
Route::get('/post', 'PostController@post')->middleware();
Route::post('/addPost', 'PostController@addPost')->middleware('auth');
Route::get('/view/{id}', 'PostController@viewPost');
Route::get('/edit/{id}', 'PostController@edit')->middleware('auth');
Route::post('/edit/{id}', 'PostController@editPost')->middleware('auth');
Route::get('/delete/{id}', 'PostController@deletePost')->middleware('auth');
Route::get('/category/{id}', 'PostController@category')->middleware('auth');
// Search
Route::post('/search', 'PostController@search');

// Likes and Dislikes
Route::get('/like/{id}', 'PostController@like');
Route::get('/dislike/{id}', 'PostController@dislike');


// CategoryController
Route::get('/category', 'CategoryController@category')->middleware('auth');
Route::post('/addCategory', 'CategoryController@addCategory')->middleware('auth');

// ProfileController
Route::get('/profile', 'ProfileController@profile')->middleware('auth');
Route::post('/addProfile', 'ProfileController@addProfile')->middleware('auth');

// Comments
Route::post('/comment/{id}', 'PostController@comment');
