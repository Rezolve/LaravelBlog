<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Auth;
use App\Profile;
use App\User;
use App\Post;
use App\Category;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
//        связывание таблиц users и profiles с проверкой авторизации
        $user_id = Auth::user()->id;
        $categories = Category::all();
        $profile = DB::table('users')
            ->join('profiles', 'users.id', '=', 'profiles.user_id')
            ->select('users.*', 'profiles.*')
            ->where(['profiles.user_id' => $user_id])
            ->first();


        $posts = Post::orderBy('id', 'DESC')->paginate(7);
//        $posts = Post::all();
        return view('home', ['profile' => $profile, 'categories' => $categories, 'posts' => $posts]);
    }
}
